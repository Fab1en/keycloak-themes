import os
import json
import shutil
import hashlib
import pathlib
import re

srcDir = "src"
buildDir = "build"
baseTheme = "liiibre"


def geThemes():
    themes = []
    for name in os.listdir(srcDir):
        types = os.listdir(os.path.join(srcDir, name))
        themes.append({"name": name, "types": types})
    return themes


def clearBuildDir():
    if os.path.exists(buildDir):
        shutil.rmtree(buildDir)
    os.mkdir(buildDir)


def generateManifest(themes):
    manifestDir = os.path.join(buildDir, "META-INF")
    os.mkdir(manifestDir)
    with open(os.path.join(manifestDir, "keycloak-themes.json"), "w+") as manifest:
        manifest.write(json.dumps({"themes": themes}))


def copyTheme(theme):
    src = os.path.join(srcDir, theme["name"])
    dest = os.path.join(buildDir, "theme", theme["name"])
    shutil.copytree(src, dest)


def hashResources(theme):
    hashedRessources = {}
    for themeType in theme["types"]:
        hashedRessources[themeType] = []
        currentDir = os.path.join(buildDir, "theme", theme["name"], themeType)
        for dirpath, _, files in os.walk(currentDir):
            for file in files:
                path = pathlib.Path(os.path.join(dirpath, file))
                if re.search(".*\.(js|css)", path.as_posix()):
                    fileHash = getFileHash(path)
                    hashedPath = suffixFile(path, fileHash)
                    convertedPaths = convertPaths(
                        currentDir, path.as_posix(), hashedPath.as_posix())
                    hashedRessources[themeType].append(convertedPaths)
    return hashedRessources


def getFileHash(path):
    with open(path, "rb") as f:
        file_hash = hashlib.md5()
        while chunk := f.read(8192):
            file_hash.update(chunk)
    return file_hash.hexdigest()


def suffixFile(path, suffix):
    suffixedPath = path.with_stem(
        path.stem + "." + suffix)
    os.rename(path, suffixedPath)
    return suffixedPath


def convertPaths(dir, *paths):
    return [path.replace(os.path.join(dir, "resources/"), "") for path in paths]


def updateProperties(theme, hashDic):
    for themeType in theme["types"]:
        if theme["name"] == baseTheme:
            toReplace = [*hashDic[baseTheme][themeType]]
        else:
            toReplace = [*hashDic[baseTheme][themeType],
                         *hashDic[theme["name"]][themeType]]
        with open(os.path.join(buildDir, "theme", theme["name"], themeType, "theme.properties"), 'r+') as f:
            content = f.read()
            for replaceArgs in toReplace:
                content = content.replace(*replaceArgs)
                f.seek(0)
                f.write(content)


if __name__ == "__main__":

    clearBuildDir()
    themes = geThemes()
    generateManifest(themes)

    hashDic = {}
    for theme in themes:
        copyTheme(theme)
        hashDic[theme["name"]] = hashResources(theme)

    for theme in themes:
        updateProperties(theme, hashDic)
