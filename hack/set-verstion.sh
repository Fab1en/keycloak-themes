#!/bin/bash

sed "s|quay.io/keycloak/keycloak:.*\$|quay.io/keycloak/keycloak:$1|" -i docker-compose.yml
sed "s|VERSION:.*\$|VERSION: \"$1\"|" -i docker-compose.prod.yml
sed "s|VERSION:.*\$|VERSION: \"$1\"|" -i .gitlab-ci.yml