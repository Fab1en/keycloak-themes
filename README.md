## Keycloak Themes

## Development
To start the project :
```
docker-compose up -d
```

To stop the project :
```
docker-compose down
``` 

To create a new theme :
1. Add the a new folder under `src/`
2. Add a volume bind in `docker-compose.yml`

## Deployment

1. Download the [latest package](https://git.indie.host/indiehost/tech/keycloak-themes/-/jobs/artifacts/master/raw/liiibre.jar?job=package) 
2. Copy it in the folder `/opt/jboss/keycloak/standalone/deployments/`