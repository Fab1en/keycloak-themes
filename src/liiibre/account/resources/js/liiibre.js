if (window.self !== window.top) {
  document.addEventListener("DOMContentLoaded", function () {
    document.body.classList.add("in-iframe");
  });
}
