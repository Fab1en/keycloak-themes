ARG VERSION
FROM quay.io/keycloak/keycloak:${VERSION} as builder
ENV KC_HEALTH_ENABLED=true
ENV KC_HTTP_RELATIVE_PATH=/auth/
ENV KC_DB=postgres
# RUN curl "https://lab.libreho.st/libre.sh/scim/keycloak-scim/-/jobs/artifacts/main/raw/build/libs/keycloak-scim-1.0-SNAPSHOT-all.jar?job=package" -Lo /opt/keycloak/providers/keycloak-scim-1.0-SNAPSHOT-all.jar
# RUN curl https://github.com/aerogear/keycloak-metrics-spi/releases/download/2.5.3/keycloak-metrics-spi-2.5.3.jar -Lo /opt/keycloak/providers/keycloak-metrics-spi-2.5.3.jar
COPY ./liiibre.jar /opt/keycloak/providers/liiibre.jar
COPY ./lescommuns/lescommuns /opt/keycloak/themes/lescommuns
COPY ./lescommuns/lescommuns-ancien /opt/keycloak/themes/lescommuns-ancien
COPY ./lescommuns/lescommuns-jdn /opt/keycloak/themes/lescommuns-jdn
COPY ./lescommuns/lescommuns-udn /opt/keycloak/themes/lescommuns-udn
COPY ./lescommuns/dfc /opt/keycloak/themes/lescommuns-dfc
COPY ./lescommuns/essaipossible /opt/keycloak/themes/lescommuns-essaipossible

COPY ./mappers.jar /opt/keycloak/providers/mappers.jar

RUN /opt/keycloak/bin/kc.sh build --features="scripts,declarative-user-profile"

ARG VERSION
FROM quay.io/keycloak/keycloak:${VERSION}
COPY --from=builder /opt/keycloak/ /opt/keycloak/
ENV DISABLE_EXTERNAL_ACCESS=true
WORKDIR /opt/keycloak
ENTRYPOINT ["/opt/keycloak/bin/kc.sh"]
CMD ["start --optimized"]
